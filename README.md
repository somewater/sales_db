Sales REST-сервис с использованием Cassandra и Akka HTTP. Тестировал с Cassandra 3.0.9.

#### Структура БД:
Исходя из производимых запросов, выбрана следующая структура таблицы sales:
```
CREATE TABLE IF NOT EXISTS sales (
   shop_id int,
   sale_day  text,
   sale_date timestamp,
   product_id int,
   product_count int,
   price decimal,
   category_id int,
   vendor_id int,

   PRIMARY KEY ((sale_day), shop_id, sale_date, price)
);
```
Т.е.:

* добавлен столбец `sale_day` (содержит день продажи, без точного времени) и использован для партиционирования.

      __Примечание:__ добавление в партиционирование столбца `shop_id` улучшило бы балансировку данных по нодам
      и увеличило бы максимальное кол-во хранимых для одного дня покупок -
      но запрос №1 (запрос всех покупок по временному диапазону) при этом был бы еще более неэффективным:
      пришлось бы читать вообще все доступные строки, сейчас же читаются все строки в пределах дней временного диапазона.

      Т.е. тут вопрос целесообразности.
      В выбранном варианте в один день можно записать не более 2/4 = 0.5 млрд записей по всем магазинам в целом.

* кластерными столбцами выбраны `shop_id`, `sale_date`, `price` - исходя из специфики запросов.
Исходя из целесообразности, можно исключить `price` из кластерных столбцов (делать по нему фильтрацию),
т.к. он необходим только для запроса №4

* запрос №3 (включает условие по product_id) делается с получением всех продуктов и аггрегацией из них нужного значения
в самом App server. При недостаточной производительности, можно рассмотреть вопрос создания аналогичной sales таблицы,
включающей product_id как кластерный ключ.

#### Старт сервера:
```
# создать стуктуру БД и заполнить её случайными данными,
# после этого можно послать к серверу запросы, аналогичные описанным ниже:
$ sbt "run create"

# старт сервера
$ sbt run

# Удалить БД
$ sbt "run drop"
```

#### Поддерживаемые запросы:

1) Все продажи за период:
```
curl -XPOST "http://localhost:3000/test/get-sales-by-period" \
  -H "Content-Type: application/json" \
  -d \
    '{"from": "2016-12-01 10:00:00", "to":"2016-12-05 12:00:00"}'
```

2) Все продажи за период в выбранных магазинах:
```
curl -XPOST "http://localhost:3000/test/get-sales-by-shop" \
 -H "Content-Type: application/json" \
 -d \
    '{"from": "2016-12-01 10:00:00", "to":"2016-12-05 12:00:00",
      "shops": [1, 2, 3]}'
```

3) Сумма продаж за период в выбранных магазинах, по выбранному диапазону товаров:
```
curl -XPOST "http://localhost:3000/test/get-sales-by-shop-product" \
  -H "Content-Type: application/json" \
  -d \
    '{"from": "2016-12-01 10:00:00", "to":"2016-12-05 12:00:00",
      "shops": [1, 2, 3], "products": [1001, 1002, 1003]}'
```

4) Наименование продуктов, проданных за указанный период в выбранных магазинах с ценой в заданном интервале:
```
curl -XPOST "http://localhost:3000/test/get-sales-by-shop-price" \
  -H "Content-Type: application/json" \
  -d \
    '{"from": "2016-12-01 10:00:00", "to":"2016-12-05 12:00:00",
      "shops": [1, 2, 3],
      "price_from": 15000.5,
      "price_to": 18000.5}'
```