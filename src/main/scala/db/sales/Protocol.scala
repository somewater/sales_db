package db.sales

import java.text.{DateFormat, SimpleDateFormat}
import java.time.format.DateTimeFormatter
import java.util.Date

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpCharsets
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import spray.json.{ParserInput, _}

/**
  * Модели данных и их сериализация
  */
object Protocol {
  private val dayFormatter = new SimpleDateFormat("yyyy-MM-dd")

  // requests
  case class SalesByPeriod(from: Date, to: Date) // -> Sales
  case class SalesByShop(from: Date, to: Date, shops: Array[Int]) // -> Sales
  case class SalesByShopProduct(from: Date, to: Date, shops: Array[Int], products: Array[Int]) // -> SumResponse
  case class SalesByShopPrice(from: Date, to: Date, shops: Array[Int], priceFrom: BigDecimal, priceTo: BigDecimal)// -> Products

  // responses
  case class Sale(shopId: Int,
                  saleDate: Date,
                  productId: Int,
                  productCount: Int,
                  price: BigDecimal,
                  categoryId: Int,
                  vendorId: Int) {
    def saleDay: String = dayFormatter.format(saleDate)
  }
  case class Sales(data: Array[Sale])
  case class Products(data: Array[Int])
  case class SumResponse(sum: BigDecimal)

  implicit object DateJson extends RootJsonFormat[Date] {
    private val parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    override def read(json: JsValue): Date = json match {
      case JsString(value) => parser.parse(value)
      case _ => throw new DeserializationException(s"Wron datetime format: $json")
    }

    override def write(obj: Date): JsValue = JsString(parser.format(obj))
  }

  object JsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {
    implicit val salesByPeriodFormat = jsonFormat2(SalesByPeriod)
    implicit val salesByShopFormat = jsonFormat3(SalesByShop)
    implicit val salesByShopProductFormat = jsonFormat4(SalesByShopProduct)
    implicit val salesByShopPriceFormat = jsonFormat(SalesByShopPrice, "from", "to", "shops", "price_from", "price_to")

    implicit val saleFormat = jsonFormat(Sale, "shop_id", "sale_date", "product_id", "product_count", "price", "category_id", "vendor_id")
    implicit val salesFormat = jsonFormat1(Sales)
    implicit val productsFormat = jsonFormat1(Products)
    implicit val sumResponseFormat = jsonFormat1(SumResponse)
  }
}
