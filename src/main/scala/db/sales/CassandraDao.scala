package db.sales

import java.text.SimpleDateFormat
import java.time.{LocalDateTime, ZoneId}
import java.time.format.DateTimeFormatter
import java.util.Date

import com.datastax.driver.core.{BatchStatement, BoundStatement, Cluster, Session}
import db.sales.Protocol.Sale

import scala.collection.convert.WrapAsJava._
import scala.collection.JavaConverters._
import com.datastax.driver.core.Row

/**
  * Вся логика работы с Cassandra
  */
class CassandraDao(cluster: Cluster, session: Session) {

  private val dayParser = DateTimeFormatter.ofPattern("yyyy-MM-dd")
  private val dayTimeParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  def createKeyspace() = {
    session.execute(
      """
        CREATE KEYSPACE IF NOT EXISTS sales_db
          WITH replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
      """)
  }

  def dropKeyspace() = {
    session.execute("DROP KEYSPACE sales_db")
  }

  def createTable(): Unit = {
    session.execute(
      """
        CREATE TABLE IF NOT EXISTS sales (
           shop_id int,
           sale_day  text,
           sale_date timestamp,
           product_id int,
           product_count int,
           price decimal,
           category_id int,
           vendor_id int,

           PRIMARY KEY ((sale_day), shop_id, sale_date, price)
        );
      """)
  }

  def insertSales(sales: Traversable[Sale]) = {
    val prepared = session.prepare("""
      INSERT INTO sales(shop_id, sale_day, sale_date, product_id, product_count, price, category_id, vendor_id)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?)
    """)
    val batch = new BatchStatement()
    sales.foreach {
      sale =>
        val st = prepared.bind().
          setInt(0, sale.shopId).
          setString(1, sale.saleDay).
          setTimestamp(2, sale.saleDate).
          setInt(3, sale.productId).
          setInt(4, sale.productCount).
          setDecimal(5, sale.price.bigDecimal).
          setInt(6, sale.categoryId).
          setInt(7, sale.vendorId)
        batch.add(st)
    }
    session.execute(batch)
  }

  def selectSalesByTime(from: Date, to: Date): Iterator[Sale] = {
    val days = dateRangeToDaysSQL(from, to)
    val rs = session.execute(
     s"""
        SELECT *
        FROM sales
        WHERE sale_day IN ($days)
      """)
    rs.iterator().asScala.map(row2Sale).filter {
      sale =>
        sale.saleDate.compareTo(from) >= 0 && sale.saleDate.compareTo(to) <= 0
    }
  }

  def selectSalesByTimeAndShops(from: Date, to: Date, shops: Traversable[Int]): Iterator[Sale] = {
    val days = dateRangeToDaysSQL(from, to)
    val rs = session.execute(
      s"""
        SELECT *
        FROM sales
        WHERE sale_day IN ($days)
          AND shop_id IN (${shops.mkString(",")})
          AND sale_date > ${toTimestamp(from)}
          AND sale_date < ${toTimestamp(to)}
      """)
    rs.iterator().asScala.map(row2Sale)
  }

  def selectPriceSum(from: Date, to: Date, shops: Traversable[Int], products: Traversable[Int]): BigDecimal = {
    val days = dateRangeToDaysSQL(from, to)
    val productsSet = products.toSet
    val rs = session.execute(
      s"""
        SELECT product_id, price, product_count
        FROM sales
        WHERE sale_day IN ($days)
          AND shop_id IN (${shops.mkString(",")})
          AND sale_date > ${toTimestamp(from)}
          AND sale_date < ${toTimestamp(to)}
      """)
    var sum: BigDecimal = BigDecimal.valueOf(0)
    rs.asScala.foreach {
      row =>
        val productId = row.getInt(0)
        if (productsSet.contains(productId)) {
          val price = row.getDecimal(1)
          val count = row.getInt(2)
          sum = sum + (BigDecimal(price) * count)
        }
    }
    sum
  }

  def selectProducts(from: Date, to: Date, shops: Traversable[Int], priceFrom0: BigDecimal, priceTo0: BigDecimal): Traversable[Int] = {
    val priceFrom = priceFrom0.bigDecimal // scala to java
    val priceTo = priceTo0.bigDecimal
    val days = dateRangeToDaysSQL(from, to)
    val rs = session.execute(
      s"""
        SELECT product_id, price
        FROM sales
        WHERE sale_day IN ($days)
          AND shop_id IN (${shops.mkString(",")})
          AND (sale_date, price) >= (${toTimestamp(from)}, $priceFrom)
          AND (sale_date, price) <= (${toTimestamp(to)}, $priceTo)
      """)
    // TODO: согласно док-и в официальном блоге http://www.datastax.com/dev/blog/a-deep-look-to-the-cql-where-clause
    // range запрос (см. "(hour, minute) >= (12, 0) AND (hour, minute) <= (14, 0)")
    // для нескольких колонок должны поддерживаться с версии 2.2,
    // однако у меня в 3.0.9 вторая часть условия (в моём случая цена) игнорировалась добавил фильтр для корректности результата
    rs.
      asScala.
      map(r => r.getInt(0) -> r.getDecimal(1)).
      filter(p => p._2.compareTo(priceFrom) >= 0 && p._2.compareTo(priceTo) <= 0).
      map(_._1).
      toSet
  }

  def close() = {
    session.close()
    cluster.close()
  }

  private def dateRangeToDaysSQL(from0: Date, to0: Date): String = {
    val from = date2LocalDateTime(from0)
    val to = date2LocalDateTime(to0)
    var i = from
    val result = collection.mutable.ListBuffer.empty[String]
    while (i.compareTo(to) <= 0) {
      result += dayParser.format(i)
      i = i.plusDays(1)
    }
    val toDay = dayParser.format(to)
    if (result.last != toDay)
      result += toDay
    result.map{d => s"'$d'"}.mkString(",")
  }

  private def date2LocalDateTime(d: Date): LocalDateTime =
    LocalDateTime.ofInstant(d.toInstant(), ZoneId.systemDefault())

  private def row2Sale(row: Row): Sale = {
    Sale(
      shopId = row.getInt("shop_id"),
      saleDate = row.getTimestamp("sale_date"),
      productId = row.getInt("product_id"),
      productCount = row.getInt("product_count"),
      price = row.getDecimal("price"),
      categoryId = row.getInt("category_id"),
      vendorId = row.getInt("vendor_id")
    )
  }

  private def toTimestamp(date: Date): String = s"'${ dayTimeParser.format(date) }'"
}
