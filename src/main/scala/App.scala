import java.text.SimpleDateFormat
import java.time.{Instant, LocalDateTime, ZoneId}
import java.util.Date

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString
import com.datastax.driver.core.exceptions.InvalidQueryException
import com.datastax.driver.core.{Cluster, Session}
import db.sales.CassandraDao
import db.sales.Protocol._

import scala.concurrent.Future
import scala.io.StdIn
import scala.util.{Failure, Random, Success}

/**
  * Точка запуска приложения, main класс. Может использоваться в нескольких режимах:
  * Для подключения сейчас используется локальная нода Cassandra.
  *
  * Создать БД, таблицу sales и заполнить её случайно сгенерированными данными:
  *   sbt "run create"
  * Запустить в режиме сервера - можно отправлять запросы (см. README):
  *   sbt "run [<host> <port>]"
  * Удалить БД:
  *   sbt "run drop"
  */
object App {
  import JsonProtocol._
  import spray.json._

  var cassandra: CassandraDao = _

  def main(args: Array[String]): Unit = {
    cassandra = startCassandraClient()

    try {
      if (args.length == 1) {
        if (args.head == "create") {
          cassandra.createKeyspace()
          cassandra.createTable()
          seedDatabase()
          println("Database sales_db created")
          return
        } else if (args.head == "drop") {
          cassandra.dropKeyspace()
          println("Database sales_db removed")
          return
        }
      }

      val (host, port) =
        if (args.length >= 2)
          (args(0), args(1).toInt)
        else if (args.length == 1)
          ("localhost", args.head.toInt)
        else ("localhost", 3000)

      runServer(host, port)
    } finally {
      cassandra.close()
    }
  }

  def startCassandraClient(): CassandraDao = {
    // TODO: read configuration from external file
    val cluster = Cluster.builder().addContactPoints("127.0.0.1").build()
    val session = try {
      cluster.connect("sales_db")
    } catch {
      case ex: InvalidQueryException =>
        val s = cluster.newSession()
        try {
          s.execute(
            """
            CREATE KEYSPACE IF NOT EXISTS sales_db
              WITH replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
            """)
        } finally {
          s.close()
        }
        cluster.connect("sales_db")
    }
    new CassandraDao(cluster, session)
  }

  def seedDatabase(count: Int = 10000): Unit = {
    val zone = ZoneId.systemDefault()
    val timeStartSec = LocalDateTime.now.atZone(zone).toEpochSecond()
    LocalDateTime.ofInstant(Instant.ofEpochMilli(1), zone)

    val r = new Random(7)
    val startDate = LocalDateTime.parse("2016-12-01T10:00:00")
    val endDate = startDate.plusDays(10)
    val startDateUnixtime = startDate.atZone(zone).toEpochSecond()
    val endDateUnixtime = endDate.atZone(zone).toEpochSecond()
    val duration: Int = (endDateUnixtime - startDateUnixtime).toInt

    val endDate2 = LocalDateTime.ofInstant(Instant.ofEpochSecond(endDateUnixtime), zone)

    val salesBatch = collection.mutable.ListBuffer.empty[Sale]
    val percent = math.max(1000, count / 100)
    var i = 0
    (0 to count).foreach { _ =>
      val saleTimeUnixtime = r.nextInt(duration) + startDateUnixtime
      val productId = 1000 + r.nextInt(1000)
      val localDate = LocalDateTime.ofInstant(Instant.ofEpochSecond(saleTimeUnixtime), zone)
      val date = Date.from(localDate.atZone(zone).toInstant())
      val sale = new Sale(
        shopId = r.nextInt(10),
        saleDate = date,
        productId = productId,
        productCount = 1 + r.nextInt(5),
        price = BigDecimal.valueOf(productId * 10),
        categoryId = 10 + productId % 100,
        vendorId = 300 + productId % 100
      )

      salesBatch += sale
      if (salesBatch.size % 100 == 0) {
        cassandra.insertSales(salesBatch.result())
        salesBatch.clear()
        if (i % percent == 0)
          println(s"Created $i of $count (${ (i.toDouble / count * 100).formatted("%.2f%%") })")
        i += 100
      }
    }

    if (salesBatch.nonEmpty)
      cassandra.insertSales(salesBatch.result())
  }

  def runServer(host: String, port: Int) = {
    implicit val system = ActorSystem("Sales")
    implicit val materializer = ActorMaterializer()
    implicit val dispatcher = system.dispatcher

    val route =
      path("test" / "get-sales-by-period") {
        post {
          entity(as[SalesByPeriod]) { request =>
            {
              val sales = cassandra.selectSalesByTime(request.from, request.to)
              complete {
                HttpResponse(
                  status = StatusCodes.OK,
                  entity = HttpEntity(ContentTypes.`application/json`, salesJsonIterator(sales)))
              }
            }
          }
        }
      } ~
      path("test" / "get-sales-by-shop") {
        post {
          entity(as[SalesByShop]) { request =>
            val sales = cassandra.selectSalesByTimeAndShops(request.from, request.to, request.shops)
            complete {
              HttpResponse(
                status = StatusCodes.OK,
                entity = HttpEntity(ContentTypes.`application/json`, salesJsonIterator(sales)))
            }
          }
        }
      } ~
      path("test" / "get-sales-by-shop-product") {
        post {
          entity(as[SalesByShopProduct]) { request =>
            val sum = cassandra.selectPriceSum(request.from, request.to, request.shops, request.products)
            complete(SumResponse(sum))
          }
        }
      } ~
      path("test" / "get-sales-by-shop-price") {
        post {
          entity(as[SalesByShopPrice]) { request =>
            val productIds = cassandra.selectProducts(request.from, request.to, request.shops, request.priceFrom, request.priceTo)
            complete(Products(productIds.toArray))
          }
        }
      }

    val bindingFuture = Http().bindAndHandle(route, host, port)
    println(s"Server online at http://$host:$port/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
  }

  // возможность потоковой генерации json-а
  private def salesJsonIterator(sales: Iterator[Sale]): Source[ByteString, Any] = {
    Source.fromIterator[ByteString] { () =>
      new Iterator[ByteString] {
        var isFirst = true

        override def hasNext: Boolean = sales.hasNext

        override def next(): ByteString = {
          val sale = sales.next()
          val isLast = !sales.hasNext
          var str =
            if (isFirst) {
              isFirst = false
              if (isLast)
                "{\"data\":[\n" + sale.toJson.prettyPrint
              else
                "{\"data\":[\n" + sale.toJson.prettyPrint + "]}"
            } else {
              if (isLast)
                ",\n" + sale.toJson.prettyPrint + "]}"
              else
                ",\n" + sale.toJson.prettyPrint
            }

          ByteString(str)
        }
      }
    }
  }
}
